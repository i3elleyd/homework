json.array!(@moviees) do |moviee|
  json.extract! moviee, :genre_id, :name, :author, :country, :publish
  json.url moviee_url(moviee, format: :json)
end
