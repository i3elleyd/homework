json.array!(@genres) do |genre|
  json.extract! genre, :id_genre, :name, :description
  json.url genre_url(genre, format: :json)
end
