class Moviee < ActiveRecord::Base
	validates :genre_id, :name, :author, :country, :publish, presence: true

	
	belongs_to :genre

	def self.search(search)
  		if search
    		find(:all, :conditions => ['name LIKE ?', "%#{search}%"])
  		else
    		find(:all)
  		end
	end
end
