require 'test_helper'

class MovieesControllerTest < ActionController::TestCase
  setup do
    @moviee = moviees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:moviees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create moviee" do
    assert_difference('Moviee.count') do
      post :create, moviee: { author: @moviee.author, country: @moviee.country, genre_id: @moviee.genre_id, name: @moviee.name, publish: @moviee.publish }
    end

    assert_redirected_to moviee_path(assigns(:moviee))
  end

  test "should show moviee" do
    get :show, id: @moviee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @moviee
    assert_response :success
  end

  test "should update moviee" do
    patch :update, id: @moviee, moviee: { author: @moviee.author, country: @moviee.country, genre_id: @moviee.genre_id, name: @moviee.name, publish: @moviee.publish }
    assert_redirected_to moviee_path(assigns(:moviee))
  end

  test "should destroy moviee" do
    assert_difference('Moviee.count', -1) do
      delete :destroy, id: @moviee
    end

    assert_redirected_to moviees_path
  end
end
