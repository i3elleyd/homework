class CreateMoviees < ActiveRecord::Migration
  def change
    create_table :moviees do |t|
      t.integer :genre_id
      t.string :name
      t.string :author
      t.string :country
      t.string :publish

      t.timestamps
    end
  end
end
