# encoding: UTF-8
 
 Moviee.create([
	{:genre_id=>1,:name=>"Mission Impossible",:author=>"Bruce Geller",:country=>"United States",:publish=>"September 17 1966"} ,
	{:genre_id=>2,:name=>"Silent Hill",:author=>"Keiichiro Toyama",:country=>"United States",:publish=>"January 31 1999"} ,
	{:genre_id=>3,:name=>"Jurassic Park",:author=>"Michael Crichton",:country=>"United States",:publish=>"June 11  1993"} ,
	{:genre_id=>7,:name=>"Mr. Popper s Penguins",:author=>"John Davis",:country=>"United States",:publish=>"June 17 2011"} ,
	{:genre_id=>5,:name=>"The Impossible",:author=>"Sergio G. Sanchez",:country=>"United States",:publish=>"September 9  2012"} ,
	{:genre_id=>6,:name=>"Twilight",:author=>"Stephenie Meyer",:country=>"United States",:publish=>"August 15  2005"} ,
])

Genre.create([
  {:name => "Action" ,:description => "สร้างความเร้าใจให้กับผู้ชมผ่านทางการใช้ความรุนแรง"},
  {:name => "Horror" ,:description => "มุ่งสร้างความกลัว ระทึกขวัญ" },
  {:name => "Adventure" ,:description => "สร้างความตื่นเต้นให้กับผู้ชมผ่านทางการเสี่ยงภัยของตัวละคร" },
  {:name => "Fantasy" ,:description => "สร้างความสนุกสนานและตระการตาตระการใจด้วยฉากและเนื้อเรื่องที่ไม่อยู่ในความเป็นจริง"},
  {:name => "Drama" ,:description => "สร้างความตื่นตัวใจ ความเศร้าสลดใจ ผ่านทางการแสดงการเติบโตของตัวละคร" },
  {:name => "Romantic" ,:description => "มุ่งสร้างความฉงนงงงวยและความรู้สึกท้าทายในการแก้ไขปริศนา" },
  {:name => "Funny" ,:description => "มุ่งสร้างความสนุกสนานและเสียงหัวเราะ"}
 ])
  
